/* config.hpp */

#pragma once

class config {
public:
	static const int num_classes;

	static const int segm_input_width;
	static const int segm_input_height;

	static const int classification_input_width;
	static const int classification_input_height;

	static const float input_mean;
	static const float input_std;

	static const std::string unet_graph;
	static const std::string resnet_graph;

	static const std::string unet_input_layer;
	static const std::string unet_output_layer;

	static const std::string resnet_input_layer;
	static const std::string resnet_output_layer;

	static const std::vector<std::string> classes;
};