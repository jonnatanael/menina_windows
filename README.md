# Menina

Program za avtomatsko segmentacijo in klasifikacijo letev (c++).

## Funkcionalnost

Program iz datoteke prebere .jpg sliko in na njej izvede segmentacijo. Detektirane regije, ki predstavljajo letve, nato izreže in klasificira. Segmentacija in klasifikacija sta izvedeni s konvolucijsko nevronsko mrežo. Za nalaganje in predikcijo nevronskih mrež program uporablja knjižnico Tensorflow, za druge operacije nad slikami pa knjižnico OpenCV.

## Knjižnice

- OpenCV 4.1.0
- Tensorflow 1.7.0
- Cuda 9.0

Knjižnica OpenCV se uporablja za medprocesiranje rezultatov segmentacijske nevronske mreže in pred uporabo nevronske mreže za klasifikacijo. Inštalacija je standardna, dobi se jo [tukaj][opencv].

Knjižnica Tensorflow skrbi za nalaganje in izvajanje nevronskih mrež. Za delovanje z našimi prednaučenimi modeli potrebujemo verzijo 1.7.0, prevedeno za uporabo v c++ programih. Prevedene datoteke so dostopne na [povezavi][tensorflow].

Knjižnica CUDA je potrebna za poganjanje nevronskih mrež na grafičnih karticah. Na voljo je na [povezavi][cuda].

## Struktura
- menina.cpp
- config.cpp
- utils.cpp
- demo.cpp

V datoteki menina.cpp je implmentiran razred menina, ki skrbi za nalaganje modelov nevronskih mrež ter pred in post procesiranje. Pri instanciranju razreda moramo podati delovni direktorij, kjer se nahajata modela za segmentacijo in klasifikacijo. Ob instanciranju se avtomatsko naložijo uteži obeh uporabljenih modelov. Zatem lahko za posamezno sliko kličemo funkcijo inference(), ki vrne seznam zaporednih številk razredov letev na sliki.

Datoteka config.cpp vsebuje konfiguracijske podatke, kot so velikost vhodnih slik za posamezno nevronsko mrežo ter seznam razredov letev, ki jih detektiramo.

V datoteki utils.cpp se nahajajo splošne funkcije, ki jih program uporablja za nalaganje slik in uteži nevronskih mrež.

V datoteki demo.cpp je enostaven primer delovanja programa. Testne slike: [slika 1][s1], [slika 2][s2], [slika 3][s3].

## Zunanji podatki

Za delovanje nevronskih mrež mora razred menina iz datotek s končnico .pb naložiti uteži. Naša rešitev uporablja dva modela, enega za segmentacijo in drugega za klasifikacijo letev. Obe datoteki s prednaučenimi utežmi sta na voljo na našem strežniku:
- [Segmentacija][segm_model]
- [Klasifikacija][class_model]

## Dodatne informacije
V repozitoriju je vključena tudi datoteka .sln, ki smo jo uporabili med razvojem. V njej so nastavljene poti do datotek, ki so nujne za delovanje programa:
- Visual Studio -> project properties -> C/C++ -> General -> Additional Include Directories
- Visual Studio -> project properties -> Linker -> General -> Additional Library Directories
- Visual Studio -> project properties -> Linker -> Input -> Additional Dependencies


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [opencv]: <https://sourceforge.net/projects/opencvlibrary/files/4.1.0/opencv-4.1.0-vc14_vc15.exe/download>
   [tensorflow]: <https://box.vicos.si/jonm/tensorflow.zip>
   [cuda]: <https://developer.nvidia.com/cuda-90-download-archive?target_os=Windows&target_arch=x86_64>
   [segm_model]: <https://box.vicos.si/jonm/unet_model.pb>
   [class_model]: <https://box.vicos.si/jonm/resnet50_best.pb>
   [s1]: <https://box.vicos.si/jonm/Cam0_104.jpg>
   [s2]: <https://box.vicos.si/jonm/Cam0_131.jpg>
   [s3]: <https://box.vicos.si/jonm/Cam0_136.jpg>
   