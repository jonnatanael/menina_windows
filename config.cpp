/* Config.cpp */
#include "pch.h" // this must be included first, always
#include "config.h"

const int config::num_classes = 10;

const int config::segm_input_width = 2000;
const int config::segm_input_height = 1024;

const int config::classification_input_width = 4000;
const int config::classification_input_height = 200;

const float config::input_mean = 128;
const float config::input_std = 255;

const std::string config::unet_graph = "unet_model.pb";
const std::string config::resnet_graph = "resnet50_best.pb";

const std::string config::unet_input_layer = "input_1";
const std::string config::unet_output_layer = "conv2d_18/BiasAdd";

const std::string config::resnet_input_layer = "input_1";
const std::string config::resnet_output_layer = "k2tfout_0";

const std::vector<std::string> config::classes = {"II", "III", "IV", "IB", "natur", "rustik", "rustik_plava", "svetli_H202", "temni_H202", "kape"};
