#include "pch.h"
#include "demo.h"

#define printTensor(T, d) \
    std::cout<< (T).tensor<float, (d)>() << std::endl

int main(int argc, char* argv[]) {
	std::string root_dir = "C:/Users/Jon/Desktop/testing/";
	std::string res_dir = root_dir + "results";

	// create output directory
	_mkdir(res_dir.c_str());

	menina m(root_dir);

	m.demo("Cam0_104");
	m.demo("Cam0_131");
	m.demo("Cam0_136");

	return 0;

}