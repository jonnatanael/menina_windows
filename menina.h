#pragma once

#include "utils.h"

class menina {

public:	
	
	menina(std::string root_dir);
	std::vector<int> inference(std::string input_name, bool verbose = false);
	void demo(std::string input_name, bool verbose=false);

private:

	std::string root_dir;
	std::unique_ptr<tensorflow::Session> unet_session;
	std::unique_ptr<tensorflow::Session> resnet_session;

	void init(bool verbose=false);
	void warmup();
	std::vector<int> get_predicted_classes(std::vector<cv::Mat> planks, std::unique_ptr<tensorflow::Session>* session, bool verbose=false);
	std::vector<cv::Mat> separate_planks(cv::Mat segm_image, std::string original_image_path, std::vector<cv::RotatedRect>& pos, bool verbose=false);
	
};