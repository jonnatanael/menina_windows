#include "pch.h"
#include "menina.h"

menina::menina(std::string root_dir) {
	this->root_dir = root_dir;
	init();
}

void menina::warmup() {
	using namespace ::tensorflow::ops;

	tensorflow::SessionOptions session_options;
	session_options.config.mutable_gpu_options()->set_allow_growth(1);

	std::unique_ptr<tensorflow::Session> sess = std::unique_ptr<tensorflow::Session>(tensorflow::NewSession(session_options));
	auto root = tensorflow::Scope::NewRootScope();
	Tensor input(tensorflow::DT_FLOAT, tensorflow::TensorShape({ 1,config::segm_input_height, config::segm_input_width,3 }));
	auto pch = Placeholder(root.WithOpName("in"), tensorflow::DT_FLOAT);
	//get_tensor_shape(input);
	std::vector<std::pair<std::string, tensorflow::Tensor>> inputs = { {"in", input}, };
	std::string output_name = "out";
	std::vector<tensorflow::Tensor> outputs;
	auto random_initializer = RandomUniform(root.WithOpName(output_name), { 1, config::segm_input_height, config::segm_input_width,3 }, tensorflow::DT_FLOAT);
	tensorflow::GraphDef graph;
	Status g = root.ToGraphDef(&graph);
	Status session_init = sess->Create(graph);
	Status stat = sess->Run({ inputs }, { output_name }, {}, &outputs);
	run_inference(&this->unet_session, config::unet_input_layer, config::unet_output_layer, outputs[0]);

	sess = std::unique_ptr<tensorflow::Session>(tensorflow::NewSession(session_options));
	root = tensorflow::Scope::NewRootScope();
	input = Tensor(tensorflow::DT_FLOAT, tensorflow::TensorShape({ 1,config::classification_input_height, config::classification_input_width,3 }));
	pch = Placeholder(root.WithOpName("in"), tensorflow::DT_FLOAT);
	inputs = { {"in", input} , };
	outputs = std::vector<tensorflow::Tensor>();
	random_initializer = RandomUniform(root.WithOpName(output_name), { 1, config::classification_input_height, config::classification_input_width,3 }, tensorflow::DT_FLOAT);
	graph = tensorflow::GraphDef();
	root.ToGraphDef(&graph);
	sess->Create(graph);
	sess->Run({ inputs }, { output_name }, {}, &outputs);
	run_inference(&this->resnet_session, config::resnet_input_layer, config::resnet_output_layer, outputs[0]);

}

void menina::init(bool verbose) {
	std::cout << "init started" << std::endl;
	//setenv("CUDA_VISIBLE_DEVICES", "0", 1);
	setenv("TF_CPP_MIN_LOG_LEVEL", "2", 1);
	tensorflow::SessionOptions session_options;
	session_options.config.mutable_gpu_options()->set_allow_growth(1);

	// U-net
	this->unet_session = std::unique_ptr<tensorflow::Session>(tensorflow::NewSession(session_options));
	std::string unet_graph_path = tensorflow::io::JoinPath(root_dir, config::unet_graph);
	Status load_graph_status = LoadGraph(unet_graph_path, &this->unet_session);
	if (verbose) {
		std::cout << "U-net graph loaded" << std::endl;
		std::cout << load_graph_status << std::endl;
	}	

	// ResNet
	this->resnet_session = std::unique_ptr<tensorflow::Session>(tensorflow::NewSession(session_options));
	std::string resnet_graph_path = tensorflow::io::JoinPath(root_dir, config::resnet_graph);
	load_graph_status = LoadGraph(resnet_graph_path, &this->resnet_session);
	if (verbose) {
		std::cout << "ResNet graph loaded" << std::endl;
		std::cout << load_graph_status << std::endl;
	}

	// warm-up the network
	warmup();
	std::cout << "init finished" << std::endl;
};

void menina::demo(std::string input_name, bool verbose) {
	// displays and saves the results
	// functionally exactly the same as menina::inference()

	int font_scale = 5;
	int font_thickness = 15;

	std::string image_path = this->root_dir + input_name + ".jpg";

	auto t1 = Clock::now();
	auto t_start = Clock::now();
	auto t2 = Clock::now();

	// load original image
	cv::Mat img_original = cv::imread(image_path);

	// load subsampled image into tensor
	t1 = Clock::now();
	Tensor img = load_image(image_path, config::segm_input_height, config::segm_input_width, config::input_mean, config::input_std);
	if (verbose)
		std::cout << "load: " << std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - t1).count() << " milliseconds" << std::endl; t1 = Clock::now();

	// perform segmentation
	Tensor segm_res = run_inference(&this->unet_session, config::unet_input_layer, config::unet_output_layer, img);
	if (verbose)
		std::cout << "segment: " << std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - t1).count() << " milliseconds" << std::endl; t1 = Clock::now();

	cv::Mat segm_image = get_segmentation_image(segm_res);

	// extract planks
	std::vector<cv::RotatedRect> pos;
	std::vector<cv::Mat> planks = separate_planks(segm_image, image_path, pos);
	if (verbose)
		std::cout << "separate: " << std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - t1).count() << " milliseconds" << std::endl; t1 = Clock::now();

	// classify planks
	std::vector<int> plank_classes = get_predicted_classes(planks, &this->resnet_session);
	if (verbose) {
		std::cout << "predict: " << std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - t1).count() << " milliseconds" << std::endl; t1 = Clock::now();
		std::cout << "timing: " << std::chrono::duration_cast<std::chrono::milliseconds>(Clock::now() - t_start).count() << " milliseconds" << std::endl;
	}

	for (int i = 0; i < pos.size(); i++) {
		std::string c = config::classes.at(plank_classes.at(i));
		cv::RotatedRect rr = pos.at(i);
		DrawRotatedRectangle(img_original, rr.center, rr.size, rr.angle);
		cv::Size rect = cv::getTextSize(c, cv::FONT_HERSHEY_SIMPLEX, font_scale, font_thickness, 0);
		putText(img_original, c, cv::Point(rr.center.x - rect.width / 2, rr.center.y + rect.height / 2), cv::FONT_HERSHEY_SIMPLEX, font_scale, cv::Scalar(255, 0, 0), font_thickness);
	}

	cv::namedWindow("demo", cv::WINDOW_NORMAL);
	imwrite(root_dir + "\\results\\" + input_name + "_demo.jpg", img_original);

	cv::imshow("demo", img_original);
	cv::waitKey(0);
}

std::vector<int> menina::inference(std::string input_name, bool verbose) {

	std::string image_path = this->root_dir + input_name + ".jpg";

	// load subsampled image
	Tensor img = load_image(image_path, config::segm_input_height, config::segm_input_width, config::input_mean, config::input_std);
	
	// perform segmentation
	Tensor segm_res = run_inference(&this->unet_session, config::unet_input_layer, config::unet_output_layer, img);
	cv::Mat segm_image = get_segmentation_image(segm_res);

	// extract planks
	std::vector<cv::RotatedRect> pos;
	std::vector<cv::Mat> planks = separate_planks(segm_image, image_path, pos);
	
	// classify planks
	std::vector<int> plank_classes = get_predicted_classes(planks, &this->resnet_session);

	return plank_classes;

};

std::vector<int> menina::get_predicted_classes(std::vector<cv::Mat> planks, std::unique_ptr<tensorflow::Session>* session, bool verbose) {
	// input: vector of plank images
	// output: vector of class indices for the input images

	std::vector<int> classes;

	for (int i = 0; i < planks.size(); i++) {

		cv::Mat cur = planks.at(i);
		//std::cout << cur.size() << std::endl;
		cv::Mat plank_resized;
		auto t1 = Clock::now();
		auto t2 = Clock::now();
		resize(cur, plank_resized, cv::Size(config::classification_input_width, config::classification_input_height));

		// convert to tensor
		tensorflow::Tensor input_tensor(tensorflow::DT_FLOAT, tensorflow::TensorShape({ 1, plank_resized.rows, plank_resized.cols, 3 }));
		auto input_tensor_mapped = input_tensor.tensor<float, 4>();

		for (int y = 0; y < plank_resized.rows; y++) {
			for (int x = 0; x < plank_resized.cols; x++) {
				cv::Vec3b pixel = plank_resized.at<cv::Vec3b>(y, x);
				input_tensor_mapped(0, y, x, 0) = (pixel.val[2] - config::input_mean) / config::input_std; //R
				input_tensor_mapped(0, y, x, 1) = (pixel.val[1] - config::input_mean) / config::input_std; //G
				input_tensor_mapped(0, y, x, 2) = (pixel.val[0] - config::input_mean) / config::input_std; //B
			}
		}

		Tensor classification_res_tensor = run_inference(session, config::resnet_input_layer, config::resnet_output_layer, input_tensor);
		int cls = get_top_index(classification_res_tensor);

		classes.push_back(cls);
		if (verbose) {
			t2 = Clock::now(); std::cout << "predicting: " << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count() << " milliseconds" << std::endl;
		}
	}

	return classes;
}

std::vector<cv::Mat> menina::separate_planks(cv::Mat segm_image, std::string original_image_path, std::vector<cv::RotatedRect>& pos, bool verbose) {
	// input: segmentation image, path to original image
	// output: list of plank images (Mat) to be used in classification

	auto t1 = Clock::now();
	auto t2 = Clock::now();

	std::vector<cv::Mat> res;
	float thr = 0.9;

	cv::Mat img_original = cv::imread(original_image_path, 1);

	segm_image *= 255;

	float f_h = (float)img_original.rows / (float)segm_image.rows;
	float f_w = (float)img_original.cols / (float)segm_image.cols;

	int erosion_size = 10;
	int erosion_type = cv::MORPH_RECT;
	cv::Mat element = getStructuringElement(erosion_type, cv::Size(2 * erosion_size + 1, 2 * erosion_size + 1), cv::Point(erosion_size, erosion_size));


	cv::Mat segm_image_new;
	erode(segm_image, segm_image_new, element);
	dilate(segm_image_new, segm_image_new, element);

	segm_image = segm_image_new;

	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;
	findContours(segm_image, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

	cv::Mat roi_mask, roi_img;
	cv::Rect2d r1, r2;
	int a, b, cc, d;
	int area_threshold = 20000;

	for (int i = 0; i < contours.size(); i++) {

		std::vector<cv::Point> c = contours.at(i);

		if (contourArea(c) < area_threshold)
			continue;

		cv::RotatedRect rr = minAreaRect(c);

		cv::Rect br = rr.boundingRect();

		// limit bounding rect to the actual image limits
		br.x = cv::max(0, br.x);
		br.y = cv::max(0, br.y);
		br.height = cv::min(segm_image.rows - br.y, br.height);
		br.width = cv::min(segm_image.cols - br.x, br.width);

		roi_mask = segm_image(br).clone() * 255;
		cv::Point2f br_center(br.x + br.width / 2, br.y + br.height / 2);

		br.x *= f_w;
		br.y *= f_h;
		br.height *= f_h;
		br.width *= f_w;

		roi_img = img_original(br).clone();
		float angle = rr.angle;
		angle = angle < -45 ? angle + 90 : angle; //correct angle if negative

		cv::Mat rot_mat1 = getRotationMatrix2D(cv::Point(roi_mask.cols / 2, roi_mask.rows / 2), angle, 1);
		cv::Mat rot_mat2 = getRotationMatrix2D(cv::Point(roi_img.cols / 2, roi_img.rows / 2), angle, 1);

		// rotate ROIs
		warpAffine(roi_mask, roi_mask, rot_mat1, roi_mask.size(), cv::INTER_CUBIC);
		warpAffine(roi_img, roi_img, rot_mat2, roi_img.size(), cv::INTER_CUBIC);

		if (rr.angle < -45) {
			rr.angle += 90;
			float tmp = rr.size.height;
			rr.size.height = rr.size.width;
			rr.size.width = tmp;
		}

		// extract another RoI from the rotated image based on rotated rectangle from contour
		cv::Rect2d unrotated_small(roi_mask.cols / 2 - rr.size.width / 2, roi_mask.rows / 2 - rr.size.height / 2, rr.size.width, rr.size.height);
		cv::Rect2d unrotated_large(roi_img.cols / 2 - rr.size.width / 2 * f_w, roi_img.rows / 2 - rr.size.height / 2 * f_h, rr.size.width * f_w, rr.size.height * f_h);
		roi_mask = roi_mask(unrotated_small);
		roi_img = roi_img(unrotated_large);

		// detect plank boundaries
		a = find_left(roi_mask, thr);
		b = find_right(roi_mask, thr);
		cc = find_top(roi_mask, thr);
		d = find_bottom(roi_mask, thr);

		// extract plank and save its location
		r1 = cv::Rect2d(a, cc, (b - a), (d - cc));
		r2 = cv::Rect2d(a * f_w, cc * f_h, (b - a) * f_w, (d - cc) * f_h);
		pos.push_back(cv::RotatedRect(cv::Point(rr.center.x * f_w, rr.center.y * f_h), cv::Size(unrotated_large.width, unrotated_large.height), rr.angle));

		roi_img = roi_img(r2); // the only important output here

		res.push_back(roi_img.clone());

	}

	return res;

}