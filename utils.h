#include "pch.h"

#include "tensorflow/core/public/session.h"
#include "tensorflow/core/lib/io/path.h"

#include <fstream>
#include <utility>
#include <vector>
#include <stdlib.h>
#include <chrono>
#include <iostream>

// OpenCV
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>

#include "config.h"

typedef std::chrono::high_resolution_clock Clock;

using tensorflow::Tensor;
using tensorflow::Status;

int setenv(const char* name, const char* value, int overwrite);
std::vector<int> get_tensor_shape(tensorflow::Tensor& tensor);
Status ReadEntireFile(tensorflow::Env* env, const std::string& filename, Tensor* output);
Status ReadTensorFromImageFile(const std::string& file_name, const int input_height, const int input_width, const float input_mean, const float input_std, std::vector<Tensor>* out_tensors);
Status LoadGraph(const std::string& graph_file_name, std::unique_ptr<tensorflow::Session>* session);
Tensor load_image(std::string image_path, int input_height, int input_width, float input_mean, float input_std);
cv::Mat get_segmentation_image(const Tensor output);
Tensor run_inference(std::unique_ptr<tensorflow::Session>* session, std::string input_name, std::string output_name, Tensor input);
int get_top_index(Tensor output);
int find_left(cv::Mat img, float thr);
int find_right(cv::Mat img, float thr);
int find_top(cv::Mat img, float thr);
int find_bottom(cv::Mat img, float thr);
void DrawRotatedRectangle(cv::Mat& image, cv::Point centerPoint, cv::Size rectangleSize, double rotationDegrees);