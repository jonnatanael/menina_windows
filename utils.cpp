#include "pch.h"
#include "utils.h"

int setenv(const char* name, const char* value, int overwrite) {
	// sets environment variables for tensorflow
	int errcode = 0;
	if (!overwrite) {
		size_t envsize = 0;
		errcode = getenv_s(&envsize, NULL, 0, name);
		if (errcode || envsize) return errcode;
	}
	return _putenv_s(name, value);
}

std::vector<int> get_tensor_shape(tensorflow::Tensor & tensor) {
	// prints dimensions of tensor
	std::vector<int> shape;
	int num_dimensions = tensor.shape().dims();
	for (int ii_dim = 0; ii_dim < num_dimensions; ii_dim++) {
		shape.push_back(tensor.shape().dim_size(ii_dim));
		std::cout << ii_dim << ": " << tensor.shape().dim_size(ii_dim) << std::endl;
	}
	return shape;
}

Status ReadEntireFile(tensorflow::Env * env, const std::string& filename, Tensor* output) {
	tensorflow::uint64 file_size = 0;
	TF_RETURN_IF_ERROR(env->GetFileSize(filename, &file_size));

	std::string contents;
	contents.resize(file_size);

	std::unique_ptr<tensorflow::RandomAccessFile> file;
	TF_RETURN_IF_ERROR(env->NewRandomAccessFile(filename, &file));

	tensorflow::StringPiece data;
	TF_RETURN_IF_ERROR(file->Read(0, file_size, &data, &(contents)[0]));
	if (data.size() != file_size) {
		return tensorflow::errors::DataLoss("Truncated read of '", filename,
			"' expected ", file_size, " got ",
			data.size());
	}
	output->scalar<std::string>()() = data.ToString();
	return Status::OK();
}

// Given an image file name, read in the data, try to decode it as an image,
// resize it to the requested size, and then scale the values as desired.
Status ReadTensorFromImageFile(const std::string& file_name, const int input_height, const int input_width, const float input_mean, const float input_std, std::vector<Tensor> * out_tensors) {
	auto root = tensorflow::Scope::NewRootScope();
	using namespace ::tensorflow::ops;  // NOLINT(build/namespaces)

	std::string input_name = "file_reader";
	std::string output_name = "normalized";

	// read file_name into a tensor named input
	Tensor input(tensorflow::DT_STRING, tensorflow::TensorShape());
	TF_RETURN_IF_ERROR(
		ReadEntireFile(tensorflow::Env::Default(), file_name, &input));

	// use a placeholder to read input data
	auto file_reader = Placeholder(root.WithOpName("input"), tensorflow::DataType::DT_STRING);

	std::vector<std::pair<std::string, tensorflow::Tensor>> inputs = {
		{"input", input},
	};

	// Now try to figure out what kind of file it is and decode it.
	const int wanted_channels = 3;
	tensorflow::Output image_reader;

	if (tensorflow::StringPiece(file_name).ends_with(".png")) {
		image_reader = DecodePng(root.WithOpName("png_reader"), file_reader, DecodePng::Channels(wanted_channels));
	}
	else if (tensorflow::StringPiece(file_name).ends_with(".gif")) {
		// gif decoder returns 4-D tensor, remove the first dim
		image_reader = Squeeze(root.WithOpName("squeeze_first_dim"), DecodeGif(root.WithOpName("gif_reader"), file_reader));
	}
	else {
		// Assume if it's neither a PNG nor a GIF then it must be a JPEG.
		image_reader = DecodeJpeg(root.WithOpName("jpeg_reader"), file_reader, DecodeJpeg::Channels(wanted_channels));
	}

	// Now cast the image data to float so we can do normal math on it.
	auto float_caster = Cast(root.WithOpName("float_caster"), image_reader, tensorflow::DT_FLOAT);

	// The convention for image ops in TensorFlow is that all images are expected
	// to be in batches, so that they're four-dimensional arrays with indices of
	// [batch, height, width, channel]. Because we only have a single image, we
	// have to add a batch dimension of 1 to the start with ExpandDims().
	auto dims_expander = ExpandDims(root, float_caster, 0);
	// Bilinearly resize the image to fit the required dimensions.
	auto resized = ResizeBilinear(root, dims_expander, Const(root.WithOpName("size"), { input_height, input_width }));
	// Subtract the mean and divide by the scale.
	Div(root.WithOpName(output_name), Sub(root, resized, { input_mean }), { input_std });

	// This runs the GraphDef network definition that we've just constructed, and
	// returns the results in the output tensor.
	tensorflow::GraphDef graph;
	TF_RETURN_IF_ERROR(root.ToGraphDef(&graph));

	std::unique_ptr<tensorflow::Session> session(tensorflow::NewSession(tensorflow::SessionOptions()));
	TF_RETURN_IF_ERROR(session->Create(graph));
	TF_RETURN_IF_ERROR(session->Run({ inputs }, { output_name }, {}, out_tensors));
	return Status::OK();
}

// Reads a model graph definition from disk, and creates a session object you
// can use to run it.
Status LoadGraph(const std::string& graph_file_name, std::unique_ptr<tensorflow::Session> * session) {
	tensorflow::GraphDef graph_def;
	Status load_graph_status =
		ReadBinaryProto(tensorflow::Env::Default(), graph_file_name, &graph_def);
	if (!load_graph_status.ok()) {
		return tensorflow::errors::NotFound("Failed to load compute graph at '", graph_file_name, "'");
	}

	/*int node_count = graph_def.node_size();
	for (int i = 0; i < node_count; i++){
		auto n = graph_def.node(i);
		std::std::cout << "Names : " << n.name() << std::std::endl;
	}*/

	session->reset(tensorflow::NewSession(tensorflow::SessionOptions()));
	Status session_create_status = (*session)->Create(graph_def);
	if (!session_create_status.ok()) {
		return session_create_status;
	}
	return Status::OK();
}

Tensor load_image(std::string image_path, int input_height, int input_width, float input_mean, float input_std) {
	std::vector<Tensor> resized_tensors;

	Status read_tensor_status = ReadTensorFromImageFile(image_path, input_height, input_width, input_mean, input_std, &resized_tensors);
	return resized_tensors[0];
}

cv::Mat get_segmentation_image(const Tensor output) {

	cv::Size s(output.shape().dim_size(2), output.shape().dim_size(1));
	cv::Mat out(s, CV_8U);

	auto res = output.tensor<float, 4>();
	for (int i = 0; i < output.shape().dim_size(1); ++i) {
		for (int j = 0; j < output.shape().dim_size(2); ++j) {
			float v0 = res(0, i, j, 0);
			float v1 = res(0, i, j, 1);
			int r;
			if (v0 > v1)
				r = 0;
			else
				r = 255;
			out.at<uchar>(i, j) = r;
		}
	}

	return out;
}

Tensor run_inference(std::unique_ptr<tensorflow::Session> * session, std::string input_name, std::string output_name, Tensor input) {

	std::vector<Tensor> outputs;
	Status run_status = (*session)->Run({ {input_name, input} }, { output_name }, {}, &outputs);
	if (!run_status.ok()) {
		LOG(ERROR) << "Running model failed: " << run_status;
	}

	return outputs[0];
}

int get_top_index(Tensor output) {

	auto res = output.tensor<float, 1>();
	int out;
	float max_score = -1;

	for (int i = 0; i < 8; i++) {
		//std::cout << to_string(i) << ": " << res(i) << std::endl;
		if (res(i) > max_score) {
			out = i;
			max_score = res(i);
		}
	}

	return out;
}

int find_left(cv::Mat img, float thr) {
	// finds leftmost nonzero pixel of binary image using binary search

	int dlim = 1;
	int w = img.cols;
	float pos1 = 0;
	float pos2 = round(w / 2);
	float pos;

	cv::Mat col = img.col(pos2);
	double s0 = sum(col)[0];
	double s;
	float d;

	while (1) {
		pos = round((pos1 + pos2) / 2);
		col = img.col(pos);
		s = sum(col)[0];

		if (s / s0 < thr) {
			pos1 = pos;
		}
		else {
			pos2 = pos;
		}

		d = abs(pos2 - pos1);

		if (d <= dlim) {
			return pos;
		}

	}

	return -1;
}

int find_right(cv::Mat img, float thr) {

	int dlim = 1;
	int w = img.cols;
	float pos1 = w;
	float pos2 = round(w / 2);
	float pos;

	cv::Mat col = img.col(pos2);
	double s0 = sum(col)[0];
	double s;
	float d;

	while (1) {
		pos = round((pos1 + pos2) / 2);
		col = img.col(pos);
		s = sum(col)[0];

		if (s / s0 < thr) {
			pos1 = pos;
		}
		else {
			pos2 = pos;
		}

		d = abs(pos2 - pos1);

		if (d <= dlim) {
			return pos;
		}

	}

	return -1;
}

int find_top(cv::Mat img, float thr) {

	int dlim = 1;
	int h = img.rows;
	float pos1 = 0;
	float pos2 = round(h / 2);
	float pos;

	cv::Mat row = img.row(pos2);
	double s0 = sum(row)[0];
	double s;
	float d;

	while (1) {
		pos = round((pos1 + pos2) / 2);
		row = img.row(pos);
		s = sum(row)[0];

		if (s / s0 < thr) {
			pos1 = pos;
		}
		else {
			pos2 = pos;
		}

		d = abs(pos2 - pos1);

		if (d <= dlim) {
			return pos;
		}
	}

	return -1;
}

int find_bottom(cv::Mat img, float thr) {

	int dlim = 1;
	int h = img.rows;
	float pos1 = h;
	float pos2 = round(h / 2);
	float pos;

	cv::Mat row = img.row(pos2);
	double s0 = sum(row)[0];
	double s;
	float d;

	while (1) {
		pos = round((pos1 + pos2) / 2);
		row = img.row(pos);
		s = sum(row)[0];

		if (s / s0 < thr) {
			pos1 = pos;
		}
		else {
			pos2 = pos;
		}

		d = abs(pos2 - pos1);

		if (d <= dlim) {
			return pos;
		}
	}

	return -1;
}



void DrawRotatedRectangle(cv::Mat& image, cv::Point centerPoint, cv::Size rectangleSize, double rotationDegrees) {
	cv::Scalar color = cv::Scalar(255, 0, 0);

	// Create the rotated rectangle
	cv::RotatedRect rotatedRectangle(centerPoint, rectangleSize, rotationDegrees);

	// We take the edges that OpenCV calculated for us
	cv::Point2f vertices2f[4];
	rotatedRectangle.points(vertices2f);

	// Convert them so we can use them in a fillConvexPoly
	cv::Point vertices[4];
	for (int i = 0; i < 4; ++i) {
		vertices[i] = vertices2f[i];

	}

	for (int i = 0; i < 4; ++i) {
		line(image, vertices[i], vertices[(i + 1) % 4], color, 3);
	}
}